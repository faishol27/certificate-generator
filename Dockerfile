FROM python:slim-buster

RUN apt update && apt upgrade && \
    apt install -y wkhtmltopdf

WORKDIR /app
COPY ./requirements.txt .
RUN pip3 install -r requirements.txt

COPY . .
RUN mkdir result preview
RUN chmod 777 result preview

EXPOSE 5000
ENTRYPOINT ["python3", "app.py"]