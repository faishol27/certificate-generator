from base64 import b64encode
from flask import Flask, request, jsonify, render_template, send_from_directory, after_this_request
from generatepdf import generate_preview, batch_single_certificate, batch_team_certificate
from io import BytesIO
from uuid import UUID
import shutil
import os
import csv

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = 'uploads/'
app.config['MAX_CONTENT_LENGTH'] = 5 * 1000 * 1000

def extract_form(certs_validate = False, csv_validate = False):
    form = request.form or request.json
    if form == None:
        raise Exception('Invalid form')
    if certs_validate and ('certs' not in request.files or request.files['certs'].filename == ""):
        raise Exception('Empty certificate template')
    if csv_validate and ('csv' not in request.files or request.files['csv'].filename == ""):
        raise Exception('Empty csv file')
    return {
        "width": form['width'] or "2000px",
        "height": form['height'] or "1440px",
        "x": form['x'] or "1626",
        "y": form['y'] or "848",
        "certs": BytesIO(request.files['certs'].read()),
        "csv": BytesIO(request.files['csv'].read())
    }

@app.route('/preview', methods=['POST'])
def preview():
    form = None
    try:
        form = extract_form(certs_validate = True)
    except Exception as e:
        return jsonify({"status": "error", "msg": str(e)}), 500
    try:
        res = generate_preview(form["certs"], form["width"], form["height"], form["x"], form["y"])
        with open(res[1], 'rb') as f:
            data = b64encode(f.read())
        shutil.rmtree(res[0])
    except Exception as e:
        return jsonify({"status": "error", "msg": str(e)}), 500
    return jsonify({"status": "ok", "data": data.decode()})

def read_csv(fcsv):
    tipe = 'SINGLE'
    ret = []

    data = fcsv.getvalue().decode().splitlines()
    csv_reader = csv.reader(data, delimiter = ',')
    if len(next(csv_reader)) > 1: tipe = 'TEAM' 
    for e in csv_reader:
        if tipe == 'TEAM':
            ret.append([e[0], e[1]])
        else:
            ret.append(e[0])
    return tipe, ret

def validation_path(s):
    try:
        val = UUID(s, version=4)
        return val.hex == s.replace('-', '')
    except:
        return False
@app.route('/download/<id>')
def download(id):
    if not validation_path(id):
        return render_template('404.html'), 404
    path = os.path.join('result', id)
    @after_this_request
    def remove_file(response):
        shutil.rmtree(path)
        os.remove(path + ".zip")
        return response
    return send_from_directory('result', path + ".zip", filename=f"{id}.zip")

@app.route('/generate', methods=['POST'])
def generate():
    form = None
    try:
        form = extract_form(certs_validate = True, csv_validate = True)
    except Exception as e:
        return jsonify({"status": "error", "msg": str(e)}), 500
    try:
        csv_res = read_csv(form['csv'])
        if csv_res[0] == 'TEAM':
            res = batch_team_certificate(csv_res[1], form["certs"], form["width"], form["height"], form["x"], form["y"])
        else:
            res = batch_single_certificate(csv_res[1], form["certs"], form["width"], form["height"], form["x"], form["y"])
    except Exception as e:
        return jsonify({"status": "error", "msg": 'Failed to generate certificate. Check your forms!'}), 500
    return jsonify({'status': "ok", "data": res[0].split('/')[1]})

@app.route('/', methods=['GET'])
def index():
    return render_template('index.html')

if __name__ == "__main__":
    PRODUCTION = os.getenv("PRODUCTION", 'true') == "false"
    app.run(host = '0.0.0.0', port = 5000, debug=not PRODUCTION)