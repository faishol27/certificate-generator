# Certificate Generator
This is simple web that will generate certificate in pdf based on svg file.

### Prerequisites
1. Python 3
2. Wkhtmltopdf

### Deployments
You can use our docker-compose to deploy this website.