from lxml import etree
from uuid import uuid4
import pathlib
import pdfkit
import shutil
import os

def generate_certificate(name, template, dirname = ".", width = "2000px", height = "1425px", x = "1626", y = "848"):
    inject_elm = etree.XML(f'<text text-anchor="middle" x="{x}" y="{y}" style="font-size:85px;fill:black;font-weight:bold;line-height:1.25;font-family:FreeSans,sans-serif;">{name}</text>')

    svg_file = etree.parse(template)
    svgroot = svg_file.getroot()
    child = svgroot.getchildren()[0] 
    child.append(inject_elm)

    with open(f"{dirname}/tmp.svg", "wb") as f:
        f.write(etree.tostring(svg_file))
    options = {
        'page-width': width,
        'page-height': height
    }

    pdfkit.from_file(f"{dirname}/tmp.svg", f"{dirname}/{name}.pdf", options)

def generate_preview(template, width = "2000px", height = "1425px", x = "1626", y = "848"):
    dirname = f"preview/{uuid4()}"
    pathlib.Path(dirname).mkdir(parents=True, exist_ok=True)
    generate_certificate("Preview", template, dirname, width, height, x, y)
    return dirname, os.path.join(dirname, "Preview.pdf")

def batch_team_certificate(datas, template, width = "2000px", height = "1425px", x = "1626", y = "848"):
    dirname = f"result/{uuid4()}"
    pathlib.Path(dirname).mkdir(parents=True, exist_ok=True)
    for e in datas:
        name = e[0]
        dirteam = dirname + e[1]
        pathlib.Path(dirteam).mkdir(parents=True, exist_ok=True)
        generate_certificate(name, template, dirteam, width, height, x, y)
    os.remove(f"{dirname}/tmp.svg")
    shutil.make_archive(f'{dirname}', 'zip', dirname)
    return dirname, dirname + ".zip"

def batch_single_certificate(names, template, width = "2000px", height = "1425px", x = "1626", y = "848"):
    dirname = f"result/{uuid4()}"
    pathlib.Path(dirname).mkdir(parents=True, exist_ok=True)
    for name in names:
        generate_certificate(name, template, dirname, width, height, x, y)
    os.remove(f"{dirname}/tmp.svg")
    shutil.make_archive(f'{dirname}', 'zip', dirname)
    return dirname, dirname + ".zip"
